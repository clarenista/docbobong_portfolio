<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    function index(){
    	return view('blog.index', ['blogs' => Blog::all()] );
    }

    function lists(){
    	return view('blog.lists', ['blogs' => Blog::all()] );
    }

    function create(){
    	return view('blog.create');
    }

    function store(Request $request){
    	$blog = new Blog();
    	$blog->title = $request->title;
    	$blog->description = $request->description;
    	$file = $request->file('file_path');

   
      // //Display File Name
      // echo 'File Name: '.$file->getClientOriginalName();
      // echo '<br>';
   
      // //Display File Extension
      // echo 'File Extension: '.$file->getClientOriginalExtension();
      // echo '<br>';
   
      // //Display File Real Path
      // echo 'File Real Path: '.$file->getRealPath();
      // echo '<br>';
   
      // //Display File Size
      // echo 'File Size: '.$file->getSize();
      // echo '<br>';
   
      // //Display File Mime Type
      // echo 'File Mime Type: '.$file->getMimeType();
   
      // //Move Uploaded File
	    $destinationPath = 'uploads/blog/';
	    $file->move($destinationPath,$file->getClientOriginalName());    	
    	$blog->file_path = 'uploads/blog/'.$file->getClientOriginalName();
        $blog->save();      
		return redirect('blog/lists')->with('status', "Blog successfully inserted.");
    }
}
