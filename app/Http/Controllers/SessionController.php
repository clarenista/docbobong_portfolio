<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sessions.index');
    }


    function lists()
    {
        return view('sessions.lists', ['sessions' => Session::all()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'g-recaptcha-response.required' => 'Please verify that you are not a robot.',
            'captcha' => 'Captcha error! try again later or contact site admin.',
        ];
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:5',
            'email' => 'required|email',
            'contact' => 'required|numeric',
            'date' => 'required|date',
            'messages' => 'required|min:5',
            'g-recaptcha-response' => 'required|captcha'
        ], $messages);
        if ($validator->fails()) {
            return redirect('sessions')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $to_email = 'bobongmd@yahoo.com';
            // $to_email = 'clarenista@gmail.com';
            $data = $request->all();
            Mail::send('emails.mail', $data, function ($message) use ($to_email) {
                $message->to($to_email)
                    ->subject('New Trial Coaching Session');
                $message->from('lifecoach.bobongmd19@gmail.com', 'New Trial Coaching Session');
            });
            $session = Session::create($request->except('_token'));
            return redirect('sessions')->with('status', "Request sent to the administrator.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        //
    }
}
