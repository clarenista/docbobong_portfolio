<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = array('name', 'email', 'contact', 'date', 'location', 'messages');
}
