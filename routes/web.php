<?php

Route::get('/', 'HomeController@index');
Route::get('/blog', 'BlogController@index');
Route::get('/mydownloadables', 'HomeController@dloadables');
Route::get('/sessions', 'SessionController@index');
Route::post('/sessions', 'SessionController@store');
Route::get('/login', 'LoginController@index');
Auth::routes([ 'register' => false ]);

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/blog/lists', 'BlogController@lists')->name('blogLists');
Route::get('/blog/create', 'BlogController@create')->name('createBlog');
Route::post('/blog/create', 'BlogController@store');

Route::get('/sessions/lists', 'SessionController@lists')->name('sessionLists');