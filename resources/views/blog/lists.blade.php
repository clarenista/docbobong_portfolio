@extends('layouts.app')
@section('content')
	<div class="container" >	

      @if (session('status'))
        <div class="alert alert-success" role="alert">
          <strong>Well done!</strong> {{ session('status') }}
        </div>
      @endif   
      <h1>Blog</h1>
      <div class="row">
        <div class="col-8">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Title</th>
                <th>View</th>
              </tr>
            </thead>
            <tbody>
              @foreach($blogs as $blog)
                <tr>
                  <td>{{$blog->title}}</td>
                  <td><a href="/{{$blog->file_path}}" class="btn btn-info" data-toggle="lightbox" data-width="1280" class="col">view</td>
                </tr>
              @endforeach
            </tbody>
          </table>           
        </div>
        <div class="col-4">
              <div class="card">
                <div class="card-body">
                  <a href="/blog/create" class="btn btn-success btn-lg">Add New</a>                     
                </div>
              </div>
        </div>
      </div>
<p></p>      
      
	</div>


@endsection

