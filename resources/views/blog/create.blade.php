@extends('layouts.app')
@section('content')
	<div class="container" >	
		<div class="card rounded-0">
		  <div class="card-body">
		    <h4 class="card-title">Create blog</h4>
		    <hr>
			<form enctype="multipart/form-data" method="post">
				@csrf
			  <fieldset>	
			    <div class="form-group">
			      <label >Title</label>
			      <input type="text" class="form-control" name="title" placeholder="Enter title">
			    </div>
				<div class="form-group">
			      <label >Description</label>
			      <textarea class="form-control" name="description" rows="3"></textarea>
			    </div>
			    <div class="form-group">
			      <label for="exampleInputFile">File input</label>
			      <input type="file" class="form-control-file" name="file_path">
			    </div>
			    <button type="submit" class="btn btn-primary">Submit</button>
			</fieldset>
			</form>			
		  </div>
		</div>		
	</div>


@endsection

