@extends('app')

@section('title')
<title>VBlog &#8226 BobongMD - Life Coach</title>
@endsection

@section('content')
<div class="container pt-5 mt-5">
	<div class="p-5">
		<h1 class="text-center mb-5">VBLOG</h1>
		<div class="row justify-content-center">
			@forelse($blogs as $blog)
			<div class="col-md-4 col-sm-12 mb-3">
				<div class="card rounded-0 shadow">
					<div class="card-body">
						<h3 class="card-title">{{$blog->title}}</h3>
						<hr>
						<a href="javascript:void(0)" id="myBtn" onclick="playVideo('{{$blog->title}}','{{$blog->file_path}}')" class="btn btn-primary">View</a>



					</div>
				</div>
			</div>

			@empty
			<div class="alert alert-warning" role="alert">
				<strong>Sorry!</strong> No blogs available.
			</div>
			@endforelse
		</div>
	</div>

	<!-- The Modal -->
	<div class="modal bd-example-modal-lg" id="myModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h2 class="modal-title" id="title"></h2>
					<button type="button" class="close" data-dismiss="modal">×</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body" id="mBody">


				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script>
	function playVideo(title, path) {
		$("#myModal").modal({
			show: true
		});
		$('#title').text(title)
		// $('#video').attr('src', '/'+path)		
		$('#mBody').append('<center><video width="320" height="240" controls id="video" controlsList="nodownload"><source src="/' + path + '" type="video/mp4" ></video></center>');

	}

	$("#myModal").on("hidden.bs.modal", function() {
		// put your default event here
		$('#mBody').empty();
	});
</script>
@endsection