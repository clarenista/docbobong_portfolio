@extends('app')
@section('title')
<title>Downloadables &#8226 BobongMD - Life Coach</title>
@endsection

@section('content')
<div class="container pt-5 mt-5">
	<div class="pt-5">
		<h1 class="text-center mb-5">DOWNLOADABLES</h1>
		<div class="row align-items-center d-flex justify-content-center">
			<div class="col-md-6 col-xs-12">
				<div class="card rounded-0 shadow mb-3">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-3">
								<img src="{{url('images/contract.png')}}" class="img-fluid" alt="contract">
							</div>
							<div class="col-9">
								<a href="{{url('downloadables/contract.pdf')}}" class="align-middle" target="_blank">
									<h2>Sample Contract taken from Breaking Through Life Coaching</h2>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


@endsection