@extends('homepage')

@section('title')
<title>BobongMD - Life Coach</title>
@endsection

@section('content')
<div class="jumbotron pt-5 mt-5 mb-0 pb-1" style="background-image: url({{ asset('images/testtube.jpg') }}); background-repeat: no-repeat; background-size: cover;">
	<div class="pt-5 mt-3">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 p-3" style="background-color: #eee">
					<center>
						<img src="{{asset('images/dp.png')}}" class="img-fluid img-thumbnail w-50" />
					</center>
				</div>
				<div class="col-md-7 col-sm-12 text-light p-4 " style="background-color: #004b67">
					<h1>Bobong Tagayuna</h1>
					<p class="lead mb-0 ">Life Coach</p>
					<p class="font-weight-bold ">Life Coach Certification (Certificate No.: 13364917), The Life Coach Training Institute</p>
					<p class="text-justify">Bobong is an accomplished medical professional. A devoted family man now in his forties ventured into his passion of life coaching. He does not claim to live a perfect life. For him life is like a game where you can win or lose. He has a winning philosophy in life and a winner in life. His vision is to make winning lives contagious by life coaching - make everybody a winner in life.</p>
				</div>
			</div>
		</div>

	</div>
	<div class="row mt-5 mb-0 h-100 justify-content-center align-items-center">
		<div class="col-6 ">


		</div>
	</div>
</div>
<div class="bg-prim p-5">
	<div class="row h-100 justify-content-center align-items-center">
		<div class="col-md-7 col-sm-12">
			<p class="text-light"><cite>Life Coaching Goal</cite></p>
			<blockquote class="blockquote text-center">
				<p class="mb-0 text-light">In five years’ time, I hope to have effectively coached all kinds of people in the general population with focus on students in crises, all sorts of entangled relationships, professional dilemmas and health-related uncertainties.</p>
			</blockquote>
			<hr class="text-light bg-light">
			<p class="text-light"><cite>Coaching Philosophy</cite></p>
			<blockquote class="blockquote text-center">
				<p class="mb-0 text-light">Life is just like a game. You are the athlete in the court. I will be your coach, you play the game – and you just keep on winning.</p>
			</blockquote>

		</div>
	</div>
</div>

@include('layouts.header')
<!-- 	<div class="container bg-dark p-4">
		<h1 class="text-center text-light">BLOG</h1>
		<div class="row justify-content-center">
			<div class="col-4">
				<a href="http://youtu.be/iQ4D273C7Ac" data-toggle="lightbox" data-gallery="youtubevideos" data-width="1280" class="col">
				    <img src="http://i1.ytimg.com/vi/iQ4D273C7Ac/mqdefault.jpg" class="img-fluid">
				</a>
			</div>
			<div class="col-4">
				<a href="http://www.youtube.com/watch?v=k6mFF3VmVAs" data-toggle="lightbox" data-gallery="youtubevideos" data-width="1280" class="col">
				    <img src="http://i1.ytimg.com/vi/yP11r5n5RNg/mqdefault.jpg" class="img-fluid">
				</a>				
			</div>
			<div class="col-4 ">
				<a href="/blog" data-toggle="lightbox" data-gallery="youtubevideos" data-width="1280" class="col">
				    <h4>See more...</h4>
				</a>				
			</div>			
		</div>
	</div> -->




@endsection