<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('title')
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
		@include('layouts.header')
        @yield('content')
		@include('layouts.footer')            		
		<script type="text/javascript" src="/js/app.js"></script>
        @yield('js')

        <script>
            $(function(){
                $('#brand').contextmenu(function(){
                    window.location.replace("/login");
                })
                
            })
        </script>
    </body>
</html>
