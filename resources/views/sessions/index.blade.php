@extends('app')
@section('title')
<title>Trial Coaching Session &#8226 BobongMD - Life Coach</title>
@endsection
@section('content')
<div class="container pt-5 mt-5">
	<div class="pt-5">
		<div class="card rounded-0 shadow">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4 col-sm-12 text-center">
						<h3>Trial Coaching Session</h3>
						<p class="lead">Schedule a Trial Coaching Session</p>
						@if (session('status'))
						<div class="alert alert-success" role="alert">
							<strong>Well done!</strong> {{ session('status') }}
						</div>
						@endif
						@if ($errors->any())
						<div class="alert alert-danger" role="alert">
							<strong>Opps!</strong> Something went wrong.
						</div>
						@endif
					</div>
					<div class="col-md-8 col-sm-12">
						<form method="post">
							@csrf
							<fieldset class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') }}" placeholder="Enter your name" required>
								@error('name')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								<label for="email">Email Address</label>
								<input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Enter your email" required>
								@error('email')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								<label for="contact">Contact No.</label>
								<input type="text" class="form-control form-control-lg @error('contact') is-invalid @enderror" name="contact" id="contact" value="{{ old('contact') }}" placeholder="Enter your contact number" required>
								@error('contact')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								<label for="date">Date</label>
								<input type="date" class="form-control form-control-lg @error('date') is-invalid @enderror" id="date" name="date" value="{{ old('date') }}" required>
								@error('date')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								<label for="location">Location</label>
								<input type="text" class="form-control form-control-lg @error('location') is-invalid @enderror" id="location" name="location" placeholder="Enter location" value="{{ old('location') }}" required>
								@error('location')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								<label for="messages">Message</label>
								<textarea class="form-control form-control-lg @error('messages') is-invalid @enderror" name="messages" id="messages" rows="3">{{ old('messages') }}</textarea>
								@error('messages')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</fieldset>
							<fieldset class="form-group">
								{!! NoCaptcha::renderJs() !!}
								{!! NoCaptcha::display() !!}
								@if ($errors->has('g-recaptcha-response'))
								<span class="help-block">
									<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
								</span>
								@endif
							</fieldset>
							<hr>
							<fieldset class="form-group">
								<button type="submit" class="btn btn-outline-success rounded-0 btn-lg" id="submitBtn"> Submit</button>
								<p id="sendTxt" style="display: none;">Sending email. Please wait.</p>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script>
	window.onload = function() {
		$('form').submit(function() {
			$('#submitBtn').attr('disabled', 'disabled')
			$('#sendTxt').show();
		})
	}
</script>
@endsection