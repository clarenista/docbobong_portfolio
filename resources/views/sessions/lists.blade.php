@extends('layouts.app')
@section('content')
<div class="container">
  <h1>Trial Session</h1>
  <div class="row">
    <div class="col-8">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Contact No.</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          @foreach($sessions as $session)
          <tr>
            <td>{{$session->name}}</td>
            <td>{{$session->email}}</td>
            <td>{{$session->contact}}</td>
            <td>{{$session->location}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <p></p>

</div>


@endsection