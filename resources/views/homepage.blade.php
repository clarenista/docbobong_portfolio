<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>BobongMD - Life Coach</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    </head>
    <body>
        @yield('content')

		@include('layouts.footer')            		
		<script type="text/javascript" src="/js/app.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
        @yield('js')
        <script>
            $(function(){
                $('#brand').contextmenu(function(){
                    window.location.replace("/login");
                })
                
            })
        </script>        
    </body>
</html>
