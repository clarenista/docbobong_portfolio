<h1>New Trial Coaching Session</h1>
<p>Good day Dr. Bobong, below are the details of a new trial coaching session request.</p>
<table id="details">
	<tbody>
		<tr>
			<td>Name:</td>
			<td><strong>{{$name}}</strong></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><strong>{{$email}}</strong></td>
		</tr>		
		<tr>
			<td>Contact Number:</td>
			<td><strong>{{$contact}}</strong></td>
		</tr>	
		<tr>
			<td>Date:</td>
			<td><strong>{{$date}}</strong></td>
		</tr>				
		<tr>
			<td>Location:</td>
			<td><strong>{{$location}}</strong></td>
		</tr>	
		<tr>
			<td>Message:</td>
			<td><strong>{{$messages}}</strong></td>
		</tr>							
	</tbody>
</table>

