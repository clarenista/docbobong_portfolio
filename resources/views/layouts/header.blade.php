  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top mb-5" style="font-size: x-large;">
    <div class="container">
      <a class="navbar-brand" href="/" id="brand">
        <h1 id="logo" class="m-0 display-4">Life Coach</h1>
        <img src="{{asset('images/bobongmd.png')}}" class="img-fluid" style="width: 200px" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-end " id="navbarColor03">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/blog">VBlog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/sessions">Trial Coaching Session</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/mydownloadables">Downloadables</a>
          </li>
        </ul>

      </div>

    </div>
  </nav>