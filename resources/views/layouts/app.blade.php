<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Life Coach Admin</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @guest

    @else

        @include('layouts.admin_header')
    @endif

    <main class="py-4">
        @yield('content')
    </main>

    <!-- Scripts -->
    <script type="text/javascript" src="/js/app.js"></script>
    
    @yield('js')
</body>
</html>



